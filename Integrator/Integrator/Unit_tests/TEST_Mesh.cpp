#include "gtest/gtest.h"

#include <iostream>
using namespace std;

#include "Mesh.h"

TEST(TEST_MESH, Reading){

	CMesh mesh;
	string mesh_filepath = "../Meshes/mesh_nx160ny_160.msh";
	
	try{
		mesh.readMeshFromFile(mesh_filepath);
	}
	catch (const std::exception& e){

		cout << e.what() << endl;
		GTEST_FAIL();
	}

	mesh.viewInFreeFEM();
}

