#include "gtest/gtest.h"

#include <iostream>
#include <vector>
using namespace std;

#include "Utilities.h"
#include "Integrator2d.h"
#include "ElastFundSolutionShifted.h"


/* todo: convergence test*/
TEST(TEST_Integrator2d, TEST_Integrator2d_singular_function_multiplied_by_fe_P0_function_convergence){
	
	vector<int> nx = { 20, 40, 80, 160, 320 };
	vector<int> ny = { 20, 40, 80, 160, 320 };

	int max_sim_count = nx.size();

	vector<double> rel_difference(max_sim_count);
	vector<double> convergence_order(max_sim_count-1);
	vector<double> convergence_order_expected = { 3.0, 0.66, 0.86, 0.93 };
	
	for (int i = 0; i < max_sim_count; i++){

		CMesh mesh;
		string nx_ny_str = CUtilities::get_nx_ny_string(nx[i], ny[i]);
		string meshFilename = "../Meshes/mesh" + nx_ny_str  + ".msh";
		mesh.readMeshFromFile(meshFilename);

		CFESpaceP0 fe_P0_function2;
		string fe_function_filepath = "../FE_functions/sin_exp_fe_P0" + nx_ny_str + ".fe";
		fe_P0_function2.read_FE_function_from_file(fe_function_filepath);

		int gauss_quadrature_order = 8;
		int* ipars = NULL;
		double fpars[2];

		CIntegrator2d integrator2d(gauss_quadrature_order);

		int iv = 100;
		fpars[0] = 0.625;// mesh.vertices[iv].x; //-4
		fpars[1] = 0.0;// mesh.vertices[iv].y; //-3

		double G = 1.7;
		double lambda = 4.5;
		CElastFundSolution::set_physical_constants(lambda, G);

		double int_2d = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmayyXi, fe_P0_function2, mesh, ipars, fpars);

		double int_exact = 0.1030823021299625; // Mathematica 9 output

		double difference = fabs(int_2d - int_exact);
		
		rel_difference[i] = difference / fabs(int_exact);
		/* Expected relative difference: */
		/*		0.0097503387588465038,if nx = ny = 20 */
		/*		0.00094187792786428468,if nx = ny = 40 */
		/*		0.00059200454922094588,if nx = ny = 80 */
		/*		0.00032612294241343492,if nx = ny = 160 */
		/*		0.00017059686139774924,if nx = ny = 320 */
	}

	for (int i = 0; i < max_sim_count - 1; i++){
		convergence_order[i] = log(rel_difference[i] / rel_difference[i + 1]) / log(2.0);

		GTEST_ASSERT_GT(convergence_order[i], convergence_order_expected[i]);
	}

	
}

