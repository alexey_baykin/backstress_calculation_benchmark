#include "gtest/gtest.h"

#include <iostream>
#include <vector>
#include <omp.h>
using namespace std;

#include "Utilities.h"
#include "Integrator2d.h"
#include "ElastFundSolutionShifted.h"


/* todo: convergence test*/
TEST(TEST_Integrator2d, TEST_Integrator2d_singular_function_multiplied_by_fe_P0_function_over_domain_convergence){

	vector<int> nx = { 20, 40, 80, 160, 320 };
	vector<int> ny = { 20, 40, 80, 160, 320 };

	int max_sim_count = nx.size()-2;

	vector<double> rel_difference(max_sim_count);
	vector<double> convergence_order(max_sim_count - 2);
	vector<double> convergence_order_expected = { 4.9, 0.39, 0.77 };

	vector<CFESpaceP1> fe_convolution(max_sim_count);

	double G = 1.7;
	double lambda = 4.5;
	CElastFundSolution::set_physical_constants(lambda, G);

	CMesh mesh_ref;
	string nx_ny_str = CUtilities::get_nx_ny_string(nx[0], ny[0]);
	string meshFilename_ref = "../Meshes/mesh" + nx_ny_str + ".msh";
	mesh_ref.readMeshFromFile(meshFilename_ref);

#pragma omp parallel num_threads(8)
	{
		for (int i = 0; i < max_sim_count; i++){

			fe_convolution[i].dofs.resize(mesh_ref.vertices.size());

#pragma omp for  schedule(dynamic)
			for (int iv = 0; iv < mesh_ref.vertices.size(); iv++){

				CMesh mesh;
				string nx_ny_str = CUtilities::get_nx_ny_string(nx[i], ny[i]);
				string meshFilename = "../Meshes/mesh" + nx_ny_str + ".msh";
				mesh.readMeshFromFile(meshFilename);

				CFESpaceP0 fe_P0_function2;
				string fe_function_filepath = "../FE_functions/sin_exp_fe_P0" + nx_ny_str + ".fe";
				fe_P0_function2.read_FE_function_from_file(fe_function_filepath);

				int gauss_quadrature_order = 8;
				int* ipars = NULL;
				double fpars[2];

				CIntegrator2d integrator2d(gauss_quadrature_order);

				//int iv = 100;
				fpars[0] = 0.625;// mesh.vertices[iv].x; //-4
				fpars[1] = 0.0;// mesh.vertices[iv].y; //-3

				fe_convolution[i].dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmayyXi, fe_P0_function2, mesh, ipars, fpars);


			}
			double int_exact = 0.1030823021299625; // Mathematica 9 output

			cout << i << " done!" << endl;

		}
	}

	for (int i = 0; i < max_sim_count - 1; i++){
		double max_abs_diff = CUtilities::calc_max_diff(fe_convolution[i].dofs, fe_convolution[i + 1].dofs);

		rel_difference[i] = max_abs_diff / CUtilities::calc_norm2(fe_convolution[i + 1].dofs);

		/* Expected relative difference: */
		/*		0.0107023,if nx1 = ny1 = 20, nx2 = ny2 = 40 */
		/*		0.000350081,if nx1 = ny1 = 40, nx2 = ny2 = 80 */
		/*		0.000265968,if nx1 = ny1 = 80, nx2 = ny2 = 160 */
		/*		0.000155553,if nx1 = ny1 = 160, nx2 = ny2 = 320 */

		cout << "max abs diff = " << max_abs_diff << endl;
		cout << "max rel diff = " << rel_difference[i] << endl;
	}

	for (int i = 0; i < max_sim_count - 2; i++){

		/* Expected convergence order difference: */
		/*		4.93409,  if nx1 = ny1 = 20, nx2 = ny2 = 40, nx3 = ny3 = 80 */
		/*		0.396433, if nx1 = ny1 = 40, nx2 = ny2 = 80, nx3 = ny3 = 160 */
		/*		0.773852, if nx1 = ny1 = 80, nx2 = ny2 = 160, nx3 = ny3 = 320 */

		convergence_order[i] = log(rel_difference[i] / rel_difference[i + 1]) / log(2.0);
		cout << "convergence order at grid #" << i << ": " << convergence_order[i] << endl;

		GTEST_ASSERT_GT(convergence_order[i], convergence_order_expected[i]);
	}

}

