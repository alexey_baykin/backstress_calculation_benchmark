#include "gtest/gtest.h"

#include <iostream>
using namespace std;

#include "Integrator2d.h"

double radius_function(double x, double y, int* ipars, double* fpars){

	return sqrt(x*x + y*y);
}

TEST(TEST_Integrator2d, TEST_Integrator2d_function_multiplied_by_fe_P1_function){

	double tol = 3.0e-5;

	CMesh mesh;
	string meshFilename = "../Meshes/mesh_nx160ny_160.msh";
	mesh.readMeshFromFile(meshFilename);

	CFESpaceP1 fe_P1_function2;
	fe_P1_function2.read_FE_function_from_file("../FE_functions/sin_exp_fe_P1_nx160ny_160.fe");

	int gauss_quadrature_order = 8;
	int* ipars = NULL;
	double* fpars = NULL;

	CIntegrator2d integrator2d(gauss_quadrature_order);

	//FreeFEM result for P0 element: -10.87375720388528.
	double int_exact = -10.873441053350168124; // Mathematica 9 output

	double int_2d =  integrator2d.calc_int2d(radius_function, fe_P1_function2, mesh, ipars, fpars);

	double difference = fabs(int_2d - int_exact);
	/* Expected relative difference: 2.9075482797739869e-005 */
	double rel_difference = difference / fabs(int_exact);

	GTEST_ASSERT_LT(rel_difference, tol);
}

