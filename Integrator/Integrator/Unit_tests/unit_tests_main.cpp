
#include "gtest/gtest.h"

int main(int argc, char **argv) {

	//::testing::FLAGS_gtest_filter = "TEST_Integrator2d.Integrate_Function";

	::testing::FLAGS_gtest_filter = "TEST_Integrator2d.TEST_Integrator2d_function_multiplied_by_fe_P0_function";

	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}