#include "gtest/gtest.h"

#include <iostream>
using namespace std;

#include "ElastFundSolution.h"

#include "Utilities.h"

TEST(TEST_ElastFundSolution, Calculation_at_point){

	double tol = 1.0e-5;

	double x_target = 2.3;
	double y_target = -1.5;

	double G = 1.7;
	double lambda = 4.5;

	CElastFundSolution::set_physical_constants(lambda, G);

	double uXi = CElastFundSolution::uXi(x_target, y_target);
	double vXi = CElastFundSolution::vXi(x_target, y_target);

	double sigmaxxXi = CElastFundSolution::sigmaxxXi(x_target, y_target);
	double sigmayyXi = CElastFundSolution::sigmayyXi(x_target, y_target);
	double sigmaxyXi = CElastFundSolution::sigmaxyXi(x_target, y_target);

	double uXi_exact = -0.106936;
	double vXi_exact = -0.0168095;

	double sigmaxxXi_exact = -0.0639104;
	double sigmayyXi_exact = -0.0122924;
	double sigmaxyXi_exact = 0.0416807;

	double uXi_rel_diff = CUtilities::get_rel_difference(uXi, uXi_exact);
	double vXi_rel_diff = CUtilities::get_rel_difference(vXi, vXi_exact);

	double sigmaxxXi_rel_diff = CUtilities::get_rel_difference(sigmaxxXi, sigmaxxXi_exact);
	double sigmayyXi_rel_diff = CUtilities::get_rel_difference(sigmayyXi, sigmayyXi_exact);
	double sigmaxyXi_rel_diff = CUtilities::get_rel_difference(sigmaxyXi, sigmaxyXi_exact);

	GTEST_ASSERT_LT(uXi_rel_diff, tol);
	GTEST_ASSERT_LT(vXi_rel_diff, tol);
	GTEST_ASSERT_LT(sigmaxxXi_rel_diff, tol);
	GTEST_ASSERT_LT(sigmayyXi_rel_diff, tol);
	GTEST_ASSERT_LT(sigmaxyXi_rel_diff, tol);

	/* Eta solution */
	double uEta = CElastFundSolution::uEta(x_target, y_target);
	double vEta = CElastFundSolution::vEta(x_target, y_target);

	double sigmaxxEta = CElastFundSolution::sigmaxxEta(x_target, y_target);
	double sigmayyEta = CElastFundSolution::sigmayyEta(x_target, y_target);
	double sigmaxyEta = CElastFundSolution::sigmaxyEta(x_target, y_target);

	double uEta_exact = -0.0168095;
	double vEta_exact = -0.121748;

	double sigmaxxEta_exact = 0.028054;
	double sigmayyEta_exact = 0.0216435;
	double sigmaxyEta_exact = -0.0331867;

	double uEta_rel_diff = CUtilities::get_rel_difference(uEta, uEta_exact);
	double vEta_rel_diff = CUtilities::get_rel_difference(vEta, vEta_exact);

	double sigmaxxEta_rel_diff = CUtilities::get_rel_difference(sigmaxxEta, sigmaxxEta_exact);
	double sigmayyEta_rel_diff = CUtilities::get_rel_difference(sigmayyEta, sigmayyEta_exact);
	double sigmaxyEta_rel_diff = CUtilities::get_rel_difference(sigmaxyEta, sigmaxyEta_exact);

	GTEST_ASSERT_LT(uEta_rel_diff, tol);
	GTEST_ASSERT_LT(vEta_rel_diff, tol);
	GTEST_ASSERT_LT(sigmaxxEta_rel_diff, tol);
	GTEST_ASSERT_LT(sigmayyEta_rel_diff, tol);
	GTEST_ASSERT_LT(sigmaxyEta_rel_diff, tol);
}