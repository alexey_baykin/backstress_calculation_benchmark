#include "gtest/gtest.h"

#include <iostream>
using namespace std;

#include "Integrator2d.h"
#include "ElastFundSolutionShifted.h"

TEST(TEST_Integrator2d, TEST_Integrator2d_singular_function_multiplied_by_fe_P0_function){

	double tol = 4.0e-4;

	CMesh mesh;
	string meshFilename = "../Meshes/mesh_nx320ny_320.msh";
	mesh.readMeshFromFile(meshFilename);

	CFESpaceP0 fe_P0_function2;
	fe_P0_function2.read_FE_function_from_file("../FE_functions/sin_exp_fe_P0_nx320ny_320.fe");

	int gauss_quadrature_order = 8;
	int* ipars = NULL;
	double fpars[2];

	CIntegrator2d integrator2d(gauss_quadrature_order);

	int iv = 100;
	fpars[0] = 0.625;// mesh.vertices[iv].x; //-4
	fpars[1] = 0.0;// mesh.vertices[iv].y; //-3

	double G = 1.7;
	double lambda = 4.5;
	CElastFundSolution::set_physical_constants(lambda,G);
	
	double int_2d = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmayyXi, fe_P0_function2, mesh, ipars, fpars);
	
	double int_exact = 0.1030823021299625; // Mathematica 9 output

	double difference = fabs(int_2d - int_exact);
	/* Expected relative difference: 0.00032612294241343492 */
	double rel_difference = difference / fabs(int_exact);

	GTEST_ASSERT_LT(rel_difference, tol);
}

