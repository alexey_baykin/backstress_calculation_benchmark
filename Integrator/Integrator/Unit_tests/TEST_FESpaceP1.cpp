#include "gtest/gtest.h"

#include <iostream>
using namespace std;

#include "FESpaceP1.h"
#include "Mesh.h"
#include "call_external_app.h"
#include "freeFEMScriptPaths.h"

TEST(TEST_FESpaceP1, Reading_fe){

	callFreeFEM(freeFEMExePath, gen_fe_P1_script_filepath, false);

	CMesh mesh;
	CFESpaceP1 fe_func;
	string mesh_filepath = freeFEMScriptFolder + "mesh_nx20ny_10.msh";
	string fe_filepath = freeFEMScriptFolder + "fe_P1_nx20ny_10.fe";
	try{
		mesh.readMeshFromFile(mesh_filepath);
		fe_func.read_FE_function_from_file(fe_filepath);
	}
	catch (const std::exception& e){

		cout << e.what() << endl;
		GTEST_FAIL();	
	}

	mesh.write_mesh_to_file(mesh_for_draw_filepath);
	fe_func.write_FE_function_to_file(fe_for_draw_filepath);

	callFreeFEM(freeFEMExePath, draw_fe_P0_script_filepath, false);

}