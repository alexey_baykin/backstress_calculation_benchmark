#include "gtest/gtest.h"

#include <iostream>
using namespace std;

#include "Integrator2d.h"

double exp_sum(double x, double y, int* ipars, double* fpars){

	return exp(x + y);
}

TEST(TEST_Integrator2d, Integrate_Function){

	double tol = 1.0e-13;

	CMesh mesh;
	string meshFilename = "../Meshes/mesh_nx160ny_160.msh";
	mesh.readMeshFromFile(meshFilename);

	int gauss_quadrature_order = 4;
	int ipars[1];
	double fpars[1];
	CIntegrator2d integrator2d(gauss_quadrature_order);
	double int_exp_exact = (exp(1) - 1)*(exp(1) - 1)*(exp(1) + 1);
	double int_exp_2d = integrator2d.calc_int2d(exp_sum, mesh, ipars, fpars);

	double difference = fabs(int_exp_2d - int_exp_exact);
	/* Expedcted relative difference: 2.6212843116889000e-014 */
	double rel_difference = difference / fabs(int_exp_exact);

	GTEST_ASSERT_LT(rel_difference, tol);
}

double singular_func(double x, double y, int* ipars, double* fpars){

	double vx = (fpars[0] - x);
	double vy = (fpars[1] - y);
	double r2 = vx*vx + vy*vy;

	return x * 1 / r2*(vx + vx*vy*vy / r2);
}

TEST(TEST_Integrator2d, Integrate_Singular_Function){

	double tol = 2.1e-5;

	CMesh mesh;
	string meshFilename = "../Meshes/mesh_nx160ny_160.msh";
	mesh.readMeshFromFile(meshFilename);

	int gauss_quadrature_order = 8;
	int ipars[1];
	double fpars[2];
	fpars[0] = 1.0 / 2.0;
	fpars[1] = 2.0;

	CIntegrator2d integrator2d(gauss_quadrature_order);
	double int_exp_exact = -0.5072242976217839;

	double int_exp_2d = integrator2d.calc_int2d(singular_func, mesh, ipars, fpars);

	double difference = fabs(int_exp_2d - int_exp_exact);
	/* Expedcted relative difference: 2.8748992931413502e-005 */
	double rel_difference = difference / fabs(int_exp_exact);

	GTEST_ASSERT_LT(difference, tol);
}

