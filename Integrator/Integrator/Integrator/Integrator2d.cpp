#include "Integrator2d.h"


CIntegrator2d::CIntegrator2d()
{
}
CIntegrator2d::CIntegrator2d(int gauss_quadrature_order_in)
{

	set_gauss_quadrature_2d(gauss_quadrature_order_in);

}

/****************************************************************************************************** */
/* This macro calculates integral of the bilinear form over thewhole mesh */
/*            int2d(ThTri)( coeff * function1(P(xi,eta),Q(xi,eta)) * function2(P(xi,eta),Q(xi,eta)) )           */
/*********************************************************************** */
double CIntegrator2d::calc_int2d(function2d function1,
	const CMesh& mesh, int* ipars1, double* fpars1){

	int nTriangles = mesh.triangles.size();

	double resultTemp;

	double result = 0.0;

	for (int i = 0; i < nTriangles; i++){

		CTriangle currentTri = mesh.triangles[i];

		/*ouputTriangleInfo(xTri,yTri); */
		resultTemp = calc_int2d_over_triangle(
						function1, 
						currentTri.xTri,
						currentTri.yTri,
						currentTri.area,
						ipars1, 
						fpars1);

		result = result + resultTemp;

		/*cout << i << " from " <<  nTriangles << endl;*/

	}
	return result;
}

/****************************************************************************************************** */
/* This function calculates integral of function over the triangle 
/*            int2d(Triangle)(  function1(P(xi,eta),Q(xi,eta)) )           
/****************************************************************************************************** */
double CIntegrator2d::calc_int2d_over_triangle(function2d function1,
	double* xTri, double* yTri, double triArea, int* ipars1, double*  fpars1){

	int nGPoints = gauss_quadrature_points;

	double result = 0.0;

	/* showGaussQuadrature(n,quadx,quady,quadw); */

	/* cout << " TriArea  = " << triArea << endl; */

	double x, y;

	for (int i = 0; i < nGPoints; i++){

		x = CMath::P(quadxi[i], quadeta[i], xTri);
		y = CMath::Q(quadxi[i], quadeta[i], yTri);

		//cout << i << " " << x << " " << y << endl;

		result = result + quadw[i] *
			function1(x, y, ipars1, fpars1);
			
	};
	result = triArea * result;

	return result;
}

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/*   Function TriGaussPoints provides the Gaussian points and weights    */
/*   for the Gaussian quadrature of order n for the standard triangles.  */
/*                                                                       */
/*   Input: n   - the order of the Gaussian quadrature (n<=12)           */
/*                                                                       */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
void CIntegrator2d::set_gauss_quadrature_2d(int gauss_quadrature_order){

	gauss_quadrature_points = get_number_gauss_points_2d(gauss_quadrature_order);

	quadxi.resize(gauss_quadrature_points);
	quadeta.resize(gauss_quadrature_points);
	quadw.resize(gauss_quadrature_points);

	switch (gauss_quadrature_order){
	case 1:
		quadxi[0] = 0.33333333333333;
		quadeta[0] = 0.33333333333333;
		quadw[0] = 1.00000000000000;
		break;

	case 2:
		quadxi[0] = 0.16666666666667;
		quadeta[0] = 0.16666666666667;
		quadw[0] = 0.33333333333333;

		quadxi[1] = 0.16666666666667;
		quadeta[1] = 0.66666666666667;
		quadw[1] = 0.33333333333333;

		quadxi[2] = 0.66666666666667;
		quadeta[2] = 0.16666666666667;
		quadw[2] = 0.33333333333333;
		break;

	case 3:

		quadxi[0] = 0.33333333333333;
		quadeta[0] = 0.33333333333333;
		quadw[0] = -0.56250000000000;

		quadxi[1] = 0.20000000000000;
		quadeta[1] = 0.20000000000000;
		quadw[1] = 0.52083333333333;

		quadxi[2] = 0.20000000000000;
		quadeta[2] = 0.60000000000000;
		quadw[2] = 0.52083333333333;

		quadxi[3] = 0.60000000000000;
		quadeta[3] = 0.20000000000000;
		quadw[3] = 0.52083333333333;
		break;

	case 4:

		quadxi[0] = 0.44594849091597;
		quadeta[0] = 0.44594849091597;
		quadw[0] = 0.22338158967801;

		quadxi[1] = 0.44594849091597;
		quadeta[1] = 0.10810301816807;
		quadw[1] = 0.22338158967801;

		quadxi[2] = 0.10810301816807;
		quadeta[2] = 0.44594849091597;
		quadw[2] = 0.22338158967801;

		quadxi[3] = 0.09157621350977;
		quadeta[3] = 0.09157621350977;
		quadw[3] = 0.10995174365532;

		quadxi[4] = 0.09157621350977;
		quadeta[4] = 0.81684757298046;
		quadw[4] = 0.10995174365532;

		quadxi[5] = 0.81684757298046;
		quadeta[5] = 0.09157621350977;
		quadw[5] = 0.10995174365532;
		break;

	case 8:

		quadxi[0] = 0.33333333333333;
		quadeta[0] = 0.33333333333333;
		quadw[0] = 0.14431560767779;

		quadxi[1] = 0.45929258829272;
		quadeta[1] = 0.45929258829272;
		quadw[1] = 0.09509163426728;

		quadxi[2] = 0.45929258829272;
		quadeta[2] = 0.08141482341455;
		quadw[2] = 0.09509163426728;

		quadxi[3] = 0.08141482341455;
		quadeta[3] = 0.45929258829272;
		quadw[3] = 0.09509163426728;

		quadxi[4] = 0.17056930775176;
		quadeta[4] = 0.17056930775176;
		quadw[4] = 0.10321737053472;

		quadxi[5] = 0.17056930775176;
		quadeta[5] = 0.65886138449648;
		quadw[5] = 0.10321737053472;

		quadxi[6] = 0.65886138449648;
		quadeta[6] = 0.17056930775176;
		quadw[6] = 0.10321737053472;

		quadxi[7] = 0.05054722831703;
		quadeta[7] = 0.05054722831703;
		quadw[7] = 0.03245849762320;

		quadxi[8] = 0.05054722831703;
		quadeta[8] = 0.89890554336594;
		quadw[8] = 0.03245849762320;

		quadxi[9] = 0.89890554336594;
		quadeta[9] = 0.05054722831703;
		quadw[9] = 0.03245849762320;

		quadxi[10] = 0.26311282963464;
		quadeta[10] = 0.72849239295540;
		quadw[10] = 0.02723031417443;

		quadxi[11] = 0.72849239295540;
		quadeta[11] = 0.00839477740996;
		quadw[11] = 0.02723031417443;

		quadxi[12] = 0.00839477740996;
		quadeta[12] = 0.26311282963464;
		quadw[12] = 0.02723031417443;

		quadxi[13] = 0.72849239295540;
		quadeta[13] = 0.26311282963464;
		quadw[13] = 0.02723031417443;

		quadxi[14] = 0.26311282963464;
		quadeta[14] = 0.00839477740996;
		quadw[14] = 0.02723031417443;

		quadxi[15] = 0.00839477740996;
		quadeta[15] = 0.72849239295540;
		quadw[15] = 0.02723031417443;

		break;

	default:

		cout << "Bad input gauss_quadrature_order" << endl;
		break;

	} //end of switch

} 

int CIntegrator2d::get_number_gauss_points_2d(int gauss_quadrature_order){

	switch (gauss_quadrature_order){
	case 1:
		return 1;
		break;

	case 2:
		return 3;
		break;

	case 3:
		return 4;
		break;

	case 4:
		return 6;
		break;

	case 8:
		return 16;
		break;

	default:
		cout << "Bad input gauss_quadrature_order" << endl;
		return 0;
		break;
	}

}

CIntegrator2d::~CIntegrator2d()
{
}
