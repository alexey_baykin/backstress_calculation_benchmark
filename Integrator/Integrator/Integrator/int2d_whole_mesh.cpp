
#include "int2d_whole_mesh.h"
#include <omp.h>

void int2d_whole_mesh(CMesh& mesh, CIntegrator2d& integrator2d, 
	CFESpaceP1& vbXi, 
	CFESpaceP1& vbEta,
	CFESpaceP0& bx, CFESpaceP0& by){

	

	int n = vbXi.dofs.size();
#pragma omp parallel num_threads(8)
	{

		double fpars[2];
#pragma omp for  schedule(dynamic)
		for (int iv = 0; iv < n; iv++){
			int* ipars = NULL;
			/* Obtain thread number */
			/*tid = omp_get_thread_num();
			printf("Hello World from thread = %d\n", tid);*/

			fpars[0] = mesh.vertices[iv].x;
			fpars[1] = mesh.vertices[iv].y;

			vbXi.dofs[iv] = 0.0;// integrator2d.calc_int2d(CElastFundSolutionShifted::vXi, bx, mesh, ipars, fpars);
			/*vbXi.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::vXi, mesh, ipars, fpars);

			sigmaxxbXi.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmaxxXi, mesh, ipars, fpars);1
			sigmayybXi.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmayyXi, mesh, ipars, fpars);
			sigmaxybXi.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmaxyXi, mesh, ipars, fpars);*/

			vbEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmayyXi, by, mesh, ipars, fpars);
			/*vbEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::vEta, mesh, ipars, fpars);

			sigmaxxbEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmaxxEta, mesh, ipars, fpars);
			sigmayybEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmayyEta, mesh, ipars, fpars);
			sigmaxybEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmaxyEta, mesh, ipars, fpars);*/

		}
	}

}