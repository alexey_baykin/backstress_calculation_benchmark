#include "Mesh.h"


CMesh::CMesh(void)
{
}


void CMesh::get_vertice_indice_with_non_zero_label(vector<int>& vertice_non_zero_label){

	vertice_non_zero_label.clear();
	vertice_non_zero_label.reserve(vertices.size());

	int label_to_exclude = 0; /* We seek vertices with non zero label */

	for (int iv = 0; iv < vertices.size(); iv++){

		if (vertices[iv].boundaryLabel != label_to_exclude){

			vertice_non_zero_label.push_back(iv);

		}

	}
}

void CMesh::get_triangle_vertices_coordinates(int k, /* Triangle number*/
	double* to_xTri, double* to_yTri ) const {

	int iglobv; /* Numder of triangle vertice in global numbering*/

	for (int iv = 0; iv<3; iv++){

		iglobv = triangles[k].verticesIndices[iv];

		to_xTri[iv] = vertices[iglobv].x; //x
		to_yTri[iv] = vertices[iglobv].y; //y

	}

}

void  CMesh::calc_triangles_vertice_coordinates_and_area(){

	int nTriangles = triangles.size();

	double xTri[3];
	double yTri[3];
	
	for (int i = 0; i < nTriangles; i++){

		get_triangle_vertices_coordinates(i, /* Triangle number*/
			xTri, yTri);

		memcpy(triangles[i].xTri, xTri, sizeof(double) * 3);
		memcpy(triangles[i].yTri, yTri, sizeof(double) * 3);
		
		triangles[i].area = CMath::calculateTriArea(xTri, yTri);

	}

}

void CMesh::readMeshFromFile(string meshFileName){

	ifstream meshFile(meshFileName);

	int vertCount,triCount,edgeCount;

	meshFile >> vertCount;
	meshFile >> triCount;
	meshFile >> edgeCount;

	vertices.resize(vertCount);
	triangles.resize(triCount);
	edges.resize(edgeCount);

	for(int i=0;i<vertCount;i++){

		meshFile >> vertices[i].x;
		meshFile >> vertices[i].y;
		meshFile >> vertices[i].boundaryLabel;

	}

	for(int i=0;i<triCount;i++){

		meshFile >> triangles[i].verticesIndices[0];
		triangles[i].verticesIndices[0]--; /* Because in freeFEM msh file numbering starts from 1 */

		meshFile >> triangles[i].verticesIndices[1];
		triangles[i].verticesIndices[1]--;

		meshFile >> triangles[i].verticesIndices[2];
		triangles[i].verticesIndices[2]--;

		meshFile >> triangles[i].regionLabel;

	}

	calc_triangles_vertice_coordinates_and_area();

	for(int i=0;i<edgeCount;i++){

		meshFile >> edges[i].verticesIndices[0];
		edges[i].verticesIndices[0]--;

		meshFile >> edges[i].verticesIndices[1];
		edges[i].verticesIndices[1]--;

		meshFile >> edges[i].boundarylabel;

	}

	meshFile.close();

};

void CMesh::write_mesh_to_file(string mesh_filepath){

	ofstream meshFile(mesh_filepath);

	meshFile.precision(16);

	meshFile << vertices.size() << " ";
	meshFile << triangles.size() << " ";
	meshFile << edges.size() << endl;

	for (size_t i = 0; i<vertices.size(); i++){

		meshFile << vertices[i].x << " ";
		meshFile << vertices[i].y << " ";
		meshFile << vertices[i].boundaryLabel << endl;

	}

	for (size_t i = 0; i<triangles.size(); i++){

		meshFile << triangles[i].verticesIndices[0] + 1 << " ";
		meshFile << triangles[i].verticesIndices[1] + 1 << " ";
		meshFile << triangles[i].verticesIndices[2] + 1 << " ";
		meshFile << triangles[i].regionLabel << endl;

	}

	for (size_t i = 0; i<edges.size(); i++){

		meshFile << edges[i].verticesIndices[0] + 1 << " ";
		meshFile << edges[i].verticesIndices[1] + 1 << " ";
		meshFile << edges[i].boundarylabel << endl;

	}

	meshFile.close();

}

void CMesh::viewInFreeFEM(){

	ofstream meshFile(mesh_for_draw_filepath);

	meshFile.precision(16);

	meshFile << vertices.size() << " ";
	meshFile << triangles.size() << " ";
	meshFile << edges.size() << endl;


	for (size_t i = 0; i<vertices.size(); i++){

		meshFile << vertices[i].x << " ";
		meshFile << vertices[i].y << " ";
		meshFile << vertices[i].boundaryLabel << endl;

	}

	for (size_t i = 0; i<triangles.size(); i++){

		meshFile << triangles[i].verticesIndices[0] + 1 << " ";
		meshFile << triangles[i].verticesIndices[1] + 1 << " ";
		meshFile << triangles[i].verticesIndices[2] + 1 << " ";
		meshFile << triangles[i].regionLabel << endl;

	}

	for (size_t i = 0; i<edges.size(); i++){

		meshFile << edges[i].verticesIndices[0] + 1 << " ";
		meshFile << edges[i].verticesIndices[1] + 1 << " ";
		meshFile << edges[i].boundarylabel << endl;

	}

	meshFile.close();

	callFreeFEM(freeFEMExePath, draw_mesh_script_filepath, false);

}



CMesh::~CMesh(void)
{
}
