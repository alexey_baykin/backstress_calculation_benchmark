#include "MathC.h"

/* This function calculates the squared distance between 2 points */

double CMath::dist2(double x1,double y1,double x2,double y2){
		
	return norm2squared(x2-x1, y2-y1);
		
}

inline double CMath::norm2squared(double x, double y){

	return x*x + y*y;

}


/* This function resizes matrix M to  M[size1][size2] */

void CMath::resizeMatrix(MatrixInt& M, int size1, int size2){

	M.resize(size1);

	for(int i=0;i<size1;i++){

		M[i].resize(size2);

	}

}

void CMath::zeroMatrix(MatrixInt& M, int size1, int size2){
	
	for(int i=0;i<size1;i++){

		fill(M[i].begin(),M[i].end(),0);

	}

}

double CMath::calculateTriArea(double* xTri, double* yTri){

	return		fabs(
                        xTri[0] * ( yTri[1] - yTri[2] )+ 
                        xTri[1] * ( yTri[2] - yTri[0] )+ 
                        xTri[2] * ( yTri[0] - yTri[1] )
                        )/2.0;

}

		
/* X-coordinate mapping from standard triangle onto arbitrary one */
/* x = P(xi,eta) */
double CMath::P(double xi, double eta,double* xTri){

	return  xTri[0] * ( 1.0 - xi - eta ) + 
			xTri[1] * xi  +
			xTri[2] * eta;
}
/* Y-coordinate mapping from standard triangle onto arbitrary one */
/* y = Q(xi,eta) */
double CMath::Q(double xi,double eta,double* yTri){

	return  yTri[0] * ( 1.0 - xi - eta ) + 
			yTri[1] * xi  +
			yTri[2] * eta;
}

/* ************************************ */
/* P1 function on standard triangle: 
/*		P1(0,0) = p0
/*		P1(1,0) = p1
/*		P1(0,1) = p2
/* ************************************ */
double CMath::P1(double xi, double eta, double* fe_P1_on_Tri){

	return  fe_P1_on_Tri[0] * (1.0 - xi - eta) +
		fe_P1_on_Tri[1] * xi +
		fe_P1_on_Tri[2] * eta;
}

	/* This function calculate the angle corresponding to (x,y), */
	/* if (x,y) is lied in the upper half-plane
	/* This adjustment is needed to make the function atan2 continuous
	/* if by the machine mistake y=0 becomes negative, y = -1e-17, for example
	/* Return the angle in range [0,pi], if y>=0 */
	
double CMath::atan3(double x,double y){

	/* The call is atan2(y,x), not atan2(x,y) */
	double angle = atan2(y,x);

	/* If y<0 then reflect the angle with respect to Ox axis */
	return angle*(y>=0.0) -angle*(y<0.0);

}

double CMath::atan4(double x,double y){

		/* The call is atan2(y,x), not atan2(x,y) */
		double angle = atan2(y,x);
		/* Another possible variant(change the branch of argument function) */
		return ( y >=0.0 ) * angle  + ( y < 0.0 ) * ( angle  + 2 * M_PI );

		
	}

void CMath::calc_vectors_sum(vector<double>& v1, vector<double>& v2, vector<double>& v_sum){

	for (int iv = 0; iv < v1.size(); iv++){

		v_sum[iv] = v1[iv] + v2[iv];

	}


}

CMath::CMath(void)
{
}


CMath::~CMath(void)
{
}
