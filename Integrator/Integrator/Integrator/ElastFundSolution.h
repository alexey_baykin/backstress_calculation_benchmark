#pragma once


#ifndef _USE_MATH_DEFINES
	#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include "MathC.h"

class CElastFundSolution
{
public:

	static double lambda;
	static double G;

	static double E;
	static double nu;

	static double nu_dash;

	static double C1;
	static double C2;
	static double C3;

	static double A1;
	static double A2;

	static double coeff;

	static void set_physical_constants(double to_lambda, double to_G);

	/* Fundamental solution with unit force in Xi direction */
	static double uXi(double x, double y);
	static double vXi(double x, double y);

	static double sigmaxxXi(double x, double y);
	static double sigmayyXi(double x, double y);
	static double sigmaxyXi(double x, double y);

	/* Fundamental solution with unit force in Eta direction */
	static double uEta(double x, double y);
	static double vEta(double x, double y);

	static double sigmaxxEta(double x, double y);
	static double sigmayyEta(double x, double y);
	static double sigmaxyEta(double x, double y);

	CElastFundSolution();
	~CElastFundSolution();
};

