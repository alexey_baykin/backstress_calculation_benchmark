#pragma once
#include "FESpace.h"

#include "Triangle.h"

#include <fstream>
class CFESpaceP1 :
	public CFESpace
{
public:
	CFESpaceP1();
	CFESpaceP1(int dofs_number);

	void get_fe_vals_in_triangle_vertices(const CTriangle& curTriangle, /* Triangle */
		double* fe_P1_on_Tri);

	virtual ~CFESpaceP1();
};

