#include "SimulationParametersPUFEM.h"


CSimulationParametersPUFEM::CSimulationParametersPUFEM(void)
{
}

void CSimulationParametersPUFEM::generateOtherParameters(){


	mu 	= E/(2.0*(1.0+nu)); 			   /* [Pa], mu elastic modulus */
	lambda = E*nu/((1.0+nu)*(1.0-2.0*nu)); /* [Pa], lambda elastic modulus */
	
	a1 = 1.0;
	a2 = mu/lambda*a1;


	/* Assign labels */

	outRLabel = 1;
	outLLabel = 2;
	outTLabel = 3;
	outFLabel = 4;
	outCLabel = 5;

	inRLabel = 6;
	inLLabel = 7;
	inTLabel = 8;
	inFLabel = 9;
	inCLabel = 10;


}


void CSimulationParametersPUFEM::writeToFile(string inputFilename ){

	ofstream inputFreeFEM(freeFEMScriptFolder + inputFilename);

	inputFreeFEM << "#R=" << endl;
	inputFreeFEM << R << endl;

	inputFreeFEM << "#kr=" << endl;
	inputFreeFEM << kr << endl;

	//Out
	inputFreeFEM << "#NoutR=" << endl;
	inputFreeFEM << NoutR << endl;

	inputFreeFEM << "#NoutL=" << endl;
	inputFreeFEM << NoutL << endl;

	inputFreeFEM << "#NoutT=" << endl;
	inputFreeFEM << NoutT << endl;

	inputFreeFEM << "#NoutF=" << endl;
	inputFreeFEM << NoutF << endl;

	inputFreeFEM << "#NoutC=" << endl;
	inputFreeFEM << NoutC << endl;
	//In
	inputFreeFEM << "#NinR=" << endl;
	inputFreeFEM << NinR << endl;

	inputFreeFEM << "#NinL=" << endl;
	inputFreeFEM << NinL << endl;

	inputFreeFEM << "#NinT=" << endl;
	inputFreeFEM << NinT << endl;

	inputFreeFEM << "#NinF=" << endl;
	inputFreeFEM << NinF << endl;

	inputFreeFEM << "#NinC=" << endl;
	inputFreeFEM << NinC << endl;

	//Labels
		//Out
	inputFreeFEM << "#outRLabel=" << endl;
	inputFreeFEM << outRLabel << endl;

	inputFreeFEM << "#outLLabel=" << endl;
	inputFreeFEM << outLLabel << endl;

	inputFreeFEM << "#outTLabel=" << endl;
	inputFreeFEM << outTLabel << endl;

	inputFreeFEM << "#outFLabel=" << endl;
	inputFreeFEM << outFLabel << endl;

	inputFreeFEM << "#outCLabel=" << endl;
	inputFreeFEM << outCLabel << endl;
		//In
	inputFreeFEM << "#inRLabel=" << endl;
	inputFreeFEM << inRLabel << endl;

	inputFreeFEM << "#NinL=" << endl;
	inputFreeFEM << inLLabel << endl;

	inputFreeFEM << "#NinT=" << endl;
	inputFreeFEM << inTLabel << endl;

	inputFreeFEM << "#NinF=" << endl;
	inputFreeFEM << inFLabel << endl;

	inputFreeFEM << "#NinC=" << endl;
	inputFreeFEM << inCLabel << endl;



	inputFreeFEM << "#a1=" << endl;
	inputFreeFEM << a1 << endl;

	inputFreeFEM << "#a2=" << endl;
	inputFreeFEM << a2 << endl;

	inputFreeFEM << "#xtip=" << endl;
	inputFreeFEM << xtip << endl;

	inputFreeFEM << "#ytip=" << endl;
	inputFreeFEM << ytip << endl;

	inputFreeFEM << "#Renr=" << endl;
	inputFreeFEM << Renr << endl;

	inputFreeFEM.close();

}


CSimulationParametersPUFEM::~CSimulationParametersPUFEM(void)
{
}
