
#include "call_external_app.h"

void callFreeFEM(string freefemExe, string freeFEMfilename,bool isWait)
{
   // additional information
   STARTUPINFO si;     
   PROCESS_INFORMATION pi;

   DWORD  exitCode;
   BOOL result;

   // set the size of the structures
   ZeroMemory( &si, sizeof(si) );
   si.cb = sizeof(si);
   ZeroMemory( &pi, sizeof(pi) );

   char lpBuffer[100];
   int nBufferLength = 100;

 //  GetCurrentDirectory(
 // nBufferLength,
 // lpBuffer
	//);

 //  string cwd = lpBuffer;

   string cwd;

   string arglist;

   string solutionRelativePath = "..\\"; //one folder up
   char solutionFullPathChar[100];

   _fullpath(solutionFullPathChar,solutionRelativePath.c_str(),100*sizeof(char));
  cwd = solutionFullPathChar;
  //isWait=true;

   /* -ne    --- no output from edp script */
   /* -v l   --- verbosity = l */

   if(isWait){

	   arglist = "\"" + freefemExe + "\" -wait -cd  \"" + cwd   + freeFEMfilename + "\"";

   }else{
   
		arglist = "\"" + freefemExe + "\" -nowait -cd  \"" + cwd   + freeFEMfilename + "\"";

   }
 
  // start the program up
   bool status = CreateProcess((LPCSTR)freefemExe.c_str(),   // the path
    (LPSTR)arglist.c_str(),        // Command line
    NULL,           // Process handle not inheritable
    NULL,           // Thread handle not inheritable
    FALSE,          // Set handle inheritance to FALSE
    CREATE_NEW_CONSOLE, 
	//CREATE_NO_WINDOW  ,             // No creation flags
    NULL,           // Use parent's environment block
    NULL,           // Use parent's starting directory 
    &si,            // Pointer to STARTUPINFO structure
    &pi );           // Pointer to PROCESS_INFORMATION structure
 
    // Close process and thread handles. 

      // Successfully created the process.  Wait for it to finish.
      WaitForSingleObject( pi.hProcess, INFINITE );
 
      // Get the exit code.
      result = GetExitCodeProcess(pi.hProcess, &exitCode);
 
      // Close the handles.
      CloseHandle( pi.hProcess );
      CloseHandle( pi.hThread );
}