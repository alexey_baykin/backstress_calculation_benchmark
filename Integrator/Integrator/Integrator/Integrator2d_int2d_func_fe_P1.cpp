#include "Integrator2d.h"

/****************************************************************************************************** */
/* This function calculates integral of function multiplied by finite P1-element function  over the whole mesh
/*
/*            int2d(Triangle)(  function1(P(xi,eta),Q(xi,eta)) * fe_P1_function2(P(xi,eta),Q(xi,eta)) )
/*
/****************************************************************************************************** */
double CIntegrator2d::calc_int2d(function2d function1, CFESpaceP1& fe_P1_function2,
	CMesh& mesh, int* ipars1, double* fpars1){

	int nTriangles = mesh.triangles.size();

	double resultTemp;

	double fe_P1_on_Tri[3];

	double result = 0.0;

	for (int i = 0; i < nTriangles; i++){

		fe_P1_function2.get_fe_vals_in_triangle_vertices(mesh.triangles[i], fe_P1_on_Tri);

		CTriangle currentTri = mesh.triangles[i];

		/*ouputTriangleInfo(xTri,yTri); */
		resultTemp = calc_int2d_over_triangle(
						function1, 
						fe_P1_on_Tri, 
						currentTri.xTri,
						currentTri.yTri,
						currentTri.area,
						ipars1, 
						fpars1);

		result = result + resultTemp;

		/*cout << i << " from " <<  nTriangles << endl;*/

	}
	return result;
}
/****************************************************************************************************** */
/* This function calculates integral of function multiplied by finite P1-element function  over the triangle
/*
/*            int2d(Triangle)(  function1(P(xi,eta),Q(xi,eta)) * fe_P1_function2(P(xi,eta),Q(xi,eta)) )
/*
/****************************************************************************************************** */
double CIntegrator2d::calc_int2d_over_triangle(function2d function1,double* fe_P1_on_Tri,
	double* xTri, double* yTri, double triArea, int* ipars1, double*  fpars1){

	double result = 0.0;

	/* showGaussQuadrature(n,quadx,quady,quadw); */

	/* cout << " TriArea  = " << triArea << endl; */

	double x, y;
	double fe_val;

	for (int i = 0; i < gauss_quadrature_points; i++){

		x = CMath::P(quadxi[i], quadeta[i], xTri);
		y = CMath::Q(quadxi[i], quadeta[i], yTri);
		fe_val = CMath::P1(quadxi[i], quadeta[i], fe_P1_on_Tri);

		result = result + quadw[i]
			* function1(x, y, ipars1, fpars1)
			* fe_val;

	};
	result = triArea * result;

	return result;
}