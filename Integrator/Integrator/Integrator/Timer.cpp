#include "Timer.h"


CTimer::CTimer()
{
}

void CTimer::start(){

	start_clock = clock();
}
double CTimer::get_time_elapsed(){

	return (clock() - start_clock) / double(CLOCKS_PER_SEC) * 1000.0;

}


CTimer::~CTimer()
{
}
