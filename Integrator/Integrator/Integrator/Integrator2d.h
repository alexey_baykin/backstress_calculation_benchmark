#pragma once
#include <vector>
#include <iostream>

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <cmath>

using namespace std;

#include "MathC.h"
#include "Mesh.h"

#include "FESpaceP1.h"
#include "FESpaceP0.h"

/* The pointer to function for integration 1d */
typedef double(*function2d)(double x, double y, int* ipars, double* fpars);

class CIntegrator2d
{
public:

	vector<double> quadxi;
	vector<double> quadeta;

	vector<double> quadw;

	int gauss_quadrature_points;

	CIntegrator2d(int gauss_quadrature_order);

	void set_gauss_quadrature_2d(int gauss_quadrature_order);
	int get_number_gauss_points_2d(int gauss_quadrature_order);

	double calc_int2d(function2d function1,
		const CMesh& mesh, int* ipars1, double* fpars1);

	double calc_int2d_over_triangle(function2d function1,
		double* xTri, double* yTri, double triArea, int* ipars1, double*  fpars1);

	double calc_int2d(function2d function1, CFESpaceP1& fe_P1_function2,
		CMesh& mesh, int* ipars1, double* fpars1);

	double calc_int2d_over_triangle(function2d function1, double* fe_P1_on_Tri,
		double* xTri, double* yTri, double triArea, int* ipars1, double*  fpars1);


	double calc_int2d(function2d function1, CFESpaceP0& fe_P0_function2,
		CMesh& mesh, int* ipars1, double* fpars1);

	double calc_int2d_over_triangle(function2d function1, double fe_P0_on_Tri,
		double* xTri, double* yTri, double triArea, int* ipars1, double*  fpars1);

	CIntegrator2d();
	virtual ~CIntegrator2d();
};

