#pragma once 

#include <iostream>
#include <vector>
#include <omp.h>
using namespace std;

#include "ElastFundSolutionShifted.h"
#include "Mesh.h"
#include "FESpaceP1.h"
#include "Integrator2d.h"
#include "Utilities.h"

#include "Timer.h"


void int2d_whole_mesh(CMesh& mesh,
	CIntegrator2d& integrator2d,
	CFESpaceP1& vbXi,
	CFESpaceP1& vbEta,
	CFESpaceP0& bx, CFESpaceP0& by);

void int2d_whole_mesh_whole_sim(
	CMesh& mesh, CIntegrator2d& integrator2d,
	CFESpaceP1& ubXi,
	CFESpaceP1& vbXi,
	CFESpaceP1& sigmaxxbXi,
	CFESpaceP1& sigmayybXi,
	CFESpaceP1& sigmaxybXi,
	CFESpaceP1& ubEta,
	CFESpaceP1& vbEta,
	CFESpaceP1& sigmaxxbEta,
	CFESpaceP1& sigmayybEta,
	CFESpaceP1& sigmaxybEta,
	CFESpaceP0& bx, CFESpaceP0& by);

void int2d_whole_mesh_whole_sim_convergence(
	CMesh& mesh, CIntegrator2d& integrator2d,
	CFESpaceP1& ubXi,
	CFESpaceP1& vbXi,
	CFESpaceP1& sigmaxxbXi,
	CFESpaceP1& sigmayybXi,
	CFESpaceP1& sigmaxybXi,
	CFESpaceP1& ubEta,
	CFESpaceP1& vbEta,
	CFESpaceP1& sigmaxxbEta,
	CFESpaceP1& sigmayybEta,
	CFESpaceP1& sigmaxybEta,
	CFESpaceP0& bx, CFESpaceP0& by);


void int2d_boundary_mesh(CMesh& mesh, 
	vector<int>& vertice_non_zero_label,
	CIntegrator2d& integrator2d,
	CFESpaceP1& vbXi,
	CFESpaceP1& vbEta,
	CFESpaceP0& bx, CFESpaceP0& by);


void whole_mesh_integration_MAIN();

void whole_mesh_int_whole_simulation_MAIN();

void boundary_mesh_integration_MAIN();

void whole_mesh_int_one_timestep_convergence_MAIN();