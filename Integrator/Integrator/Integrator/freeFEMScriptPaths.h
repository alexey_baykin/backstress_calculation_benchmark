#pragma once 
#include "allInclude.h"

#include <string>

using namespace std;

const string freeFEMScriptFolder = "..\\FreeFEMScripts\\";
const string free_FEM_script_folder_no_dots = "FreeFEMScripts\\";

/* FreeFEM scripts*/
const string draw_mesh_script_filepath = free_FEM_script_folder_no_dots + "draw_mesh_cpp.edp";
const string gen_fe_P1_script_filepath = free_FEM_script_folder_no_dots + "gen_fe_P1_cpp.edp";
const string draw_fe_P1_script_filepath = free_FEM_script_folder_no_dots + "draw_fe_P1.edp";
const string draw_fe_P0_script_filepath = free_FEM_script_folder_no_dots + "draw_fe_P0.edp";

/* FreeFEM files*/
const string mesh_for_draw_filepath = freeFEMScriptFolder + "mesh_for_draw.msh";
const string fe_for_draw_filepath = freeFEMScriptFolder + "fe_for_draw.fe";