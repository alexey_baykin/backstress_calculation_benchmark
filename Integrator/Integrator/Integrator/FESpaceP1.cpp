#include "FESpaceP1.h"


CFESpaceP1::CFESpaceP1()
{
}
CFESpaceP1::CFESpaceP1(int dofs_number)
{
	dofs.resize(dofs_number);
}

void CFESpaceP1::get_fe_vals_in_triangle_vertices( const CTriangle& curTriangle, /* Triangle */
	double* fe_P1_on_Tri){

	int iglobv; /* Numder of triangle vertice in global numbering*/

	for (int iv = 0; iv<3; iv++){

		iglobv = curTriangle.verticesIndices[iv];

		fe_P1_on_Tri[iv] = dofs[iglobv]; //x

	}

}

CFESpaceP1::~CFESpaceP1()
{
}
