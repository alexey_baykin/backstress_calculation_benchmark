

#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <numeric>
#include <algorithm>
using namespace std;

#include "Vertex.h"
#include "Triangle.h"
#include "Edge.h"
#include "MathC.h"
//#include "SimulationParameters.h"
//#include "SimulationParametersPUFEM.h"
#include "call_external_app.h"

#include "SortTwoVectors.h"

#include "freeFEMScriptPaths.h"

//#include "TriangleLib.h"
//#include "MathC.h"

#include <sstream>

//#include <unordered_map>
//#include <set>

#include <assert.h>

	template <typename T>
	std::string to_string_with_precision2(const T a_value, const int n = 6)
	{
		std::ostringstream out;
		out.precision(n);
		out << std::fixed << a_value;
		return out.str();
	}

class CMesh
{
public:

	vector<CVertex> vertices;
	vector<CTriangle> triangles;
	vector<CEdge> edges;

	CMesh(void);


	void readMeshFromFile(string meshFileName);
	void write_mesh_to_file(string mesh_filepath);
	void viewInFreeFEM();

	void get_triangle_vertices_coordinates(int k, /* Triangle number*/
		double* to_xTri, double* to_yTri) const;
	

	void calc_triangles_vertice_coordinates_and_area();


	void get_vertice_indice_with_non_zero_label(vector<int>& vertice_non_zero_label);
	
	
	~CMesh(void);
};

