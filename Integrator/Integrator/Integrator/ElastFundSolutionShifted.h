#pragma once

#include "ElastFundSolution.h"

class CElastFundSolutionShifted
{
public:

	static double uXi(double x, double y, int* ipars, double* fpars);
	static double vXi(double x, double y, int* ipars, double* fpars);

	static double sigmaxxXi(double x, double y, int* ipars, double* fpars);
	static double sigmayyXi(double x, double y, int* ipars, double* fpars);
	static double sigmaxyXi(double x, double y, int* ipars, double* fpars);

	static double uEta(double x, double y, int* ipars, double* fpars);
	static double vEta(double x, double y, int* ipars, double* fpars);

	static double sigmaxxEta(double x, double y, int* ipars, double* fpars);
	static double sigmayyEta(double x, double y, int* ipars, double* fpars);
	static double sigmaxyEta(double x, double y, int* ipars, double* fpars);


	CElastFundSolutionShifted();
	~CElastFundSolutionShifted();
};

