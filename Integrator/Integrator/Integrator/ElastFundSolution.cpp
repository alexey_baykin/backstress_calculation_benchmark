#include "ElastFundSolution.h"

/* Static members definition */
double CElastFundSolution::lambda = 1.0;
double CElastFundSolution::G = 1.0;

double CElastFundSolution::E = 1.0;
double CElastFundSolution::nu = 1.0;

double CElastFundSolution::nu_dash = 1.0;

double CElastFundSolution::C1 = 1.0;
double CElastFundSolution::C2 = 1.0;
double CElastFundSolution::C3 = 1.0;

double CElastFundSolution::A1 = 1.0;
double CElastFundSolution::A2 = 1.0;

double CElastFundSolution::coeff = 1.0;


CElastFundSolution::CElastFundSolution()
{
}

void CElastFundSolution::set_physical_constants(double to_lambda, double to_G){

	G = to_G;
	lambda = to_lambda;

	E = G * ( 3.0*lambda + 2.0*G ) / (lambda + G);
	nu = lambda / ( 2.0*(lambda + G) );
	nu_dash = nu / (1.0 - nu);

	A1 = -(1.0 + nu_dash) / (4.0*M_PI);
	A2 = (1.0 - nu_dash) / (1.0 + nu_dash);

	C1 = 3.0 - nu_dash;
	C2 = 1.0 + nu_dash;
	C3 = (7.0 - nu_dash) / 2.0;
	coeff = 1.0 / (8.0*M_PI*G);

}
/* Fundamental solution with unit force in Xi direction */
double CElastFundSolution::uXi(double x, double y){

	double r2 = CMath::norm2squared(x,y);

	return -coeff*( C1 / 2.0 * log(r2) - C2*x*x / r2 + C3 );
 
}
double CElastFundSolution::vXi(double x, double y){

	double r2 = CMath::norm2squared(x, y);

	return coeff*C2*x*y / r2;
}
double CElastFundSolution::sigmaxxXi(double x, double y){

	double r2 = CMath::norm2squared(x, y);

	return A1 / r2 * ( A2*x + 2.0*x*x*x / r2 );
}

double CElastFundSolution::sigmayyXi(double x, double y){

	//double r =sqrt( CMath::norm2squared(x, y));


	double r2 = CMath::norm2squared(x, y);
	return A1 / r2 * ( -A2*x + 2.0*x*y*y / r2 );
}

double CElastFundSolution::sigmaxyXi(double x, double y){

	double r2 = CMath::norm2squared(x, y);

	return A1 / r2 * ( A2*y + 2.0*x*x*y / r2 );
}

/* Fundamental solution with unit force in Eta direction */
double CElastFundSolution::uEta(double x, double y){

	double r2 = CMath::norm2squared(x, y);

	return coeff*C2*x*y / r2;

}
double CElastFundSolution::vEta(double x, double y){

	double r2 = CMath::norm2squared(x, y);

	return -coeff * ( C1 / 2.0 * log(r2) - C2*y*y / r2 + C3 );
}
double CElastFundSolution::sigmaxxEta(double x, double y){

	double r2 = CMath::norm2squared(x, y);

	return A1 / r2 * ( -A2*y + 2.0*x*x*y / r2 );
}

double CElastFundSolution::sigmayyEta(double x, double y){

	double r2 = CMath::norm2squared(x, y);

	return A1 / r2 * ( A2*y + 2.0*y*y*y / r2 );
}

double CElastFundSolution::sigmaxyEta(double x, double y){

	double r2 = CMath::norm2squared(x, y);

	return A1 / r2 * ( A2*x + 2.0*x*y*y / r2 );
}

CElastFundSolution::~CElastFundSolution()
{
}
