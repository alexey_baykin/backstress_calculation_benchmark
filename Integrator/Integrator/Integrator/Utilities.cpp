#include "Utilities.h"


CUtilities::CUtilities()
{
}

double CUtilities::get_rel_difference(double u_num, double u_exact){

	double difference = fabs(u_num - u_exact);
	return difference / fabs(u_exact);

}

string CUtilities::get_nx_ny_string(int nx, int ny){

	stringstream str_stream;

	str_stream << "_nx" << nx << "ny_" << ny;

	return str_stream.str();
}

string CUtilities::int_2_str(int integer){

	stringstream str_stream;

	str_stream << integer;

	return str_stream.str();
}

double CUtilities::calc_max_diff(const vector<double>& v1, const vector<double>& v2){

	if (v1.size() != v2.size()){
		return -10000000000;
	}

	double max_abs_diff = -100;
	for (int i = 0; i < v1.size(); i++){

		double abs_diff = fabs(v1[i] - v2[i]);
		if (abs_diff > max_abs_diff){
			max_abs_diff = abs_diff;
		}

	}
	return max_abs_diff;

}

double CUtilities::calc_norm2(const vector<double>& v){

	double max_norm = -100;
	for (int i = 0; i < v.size(); i++){

		double norm = fabs(v[i]);
		if (norm > max_norm){
			max_norm = norm;
		}

	}
	return max_norm;

}


CUtilities::~CUtilities()
{
}
