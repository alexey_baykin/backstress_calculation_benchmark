// Integrator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <vector>
#include <omp.h>

#include <direct.h>
using namespace std;

#include "ElastFundSolutionShifted.h"
#include "Mesh.h"
#include "FESpaceP1.h"
#include "Integrator2d.h"
#include "Utilities.h"

#include "Timer.h"

#include "int2d_whole_mesh.h"


int _tmain(int argc, _TCHAR* argv[])
{

	//boundary_mesh_integration_MAIN();
	//whole_mesh_integration_MAIN();
	whole_mesh_int_whole_simulation_MAIN();

	//whole_mesh_int_one_timestep_convergence_MAIN();

	return 0;
}

