#pragma once

#include <math.h>
#include <vector>
#include <sstream>
#include <string>
using namespace std;
class CUtilities
{
public:


	static double get_rel_difference(double u_num, double u_exact);

	static string get_nx_ny_string(int nx, int ny);

	static string int_2_str(int integer);

	static double calc_max_diff(const vector<double>& v1, const vector<double>& v2);
	static double calc_norm2(const vector<double>& v);

	CUtilities();
	~CUtilities();
};

