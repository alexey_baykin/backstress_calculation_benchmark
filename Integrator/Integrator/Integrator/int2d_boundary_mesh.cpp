
#include "int2d_whole_mesh.h"
#include <omp.h>

void int2d_boundary_mesh(CMesh& mesh, 
	vector<int>& vertice_non_zero_label,
	CIntegrator2d& integrator2d,
	CFESpaceP1& vbXi,
	CFESpaceP1& vbEta,
	CFESpaceP0& bx, CFESpaceP0& by){

	

	//int n = ubXi.dofs.size();
#pragma omp parallel num_threads(8)
	{

		double fpars[2];

#pragma omp for  schedule(dynamic)
		for (int i = 0; i < vertice_non_zero_label.size(); i++){

			int* ipars = NULL;
			/* Obtain thread number */
			/*tid = omp_get_thread_num();
			printf("Hello World from thread = %d\n", tid);*/

			int iv = vertice_non_zero_label[i];

			fpars[0] = mesh.vertices[iv].x;
			fpars[1] = mesh.vertices[iv].y;

			vbXi.dofs[iv] = 0.0;// integrator2d.calc_int2d(CElastFundSolutionShifted::vXi, bx, mesh, ipars, fpars);

			vbEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::vEta, by, mesh, ipars, fpars);

		}
	}

}