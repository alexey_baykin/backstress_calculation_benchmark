#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#include "allInclude.h"
#include "freeFEMScriptPaths.h"

class CSimulationParameters
{
public:

	double R; /* Domain radius*/
	double kr;
	double kl;
	int NN;

	double E;  /* [Pa], Young's modulus */
	double nu; /* [], Poisson's ration */

	double mu;
	double lambda;

	double a1;
	double a2;
	int Nc;
	int Nv;
	int Nf;
	int Nr;

	double xtip;
	double ytip;
	double Renr;

	CSimulationParameters(void);

	void generateOtherParameters();
	void generateOtherParameters2();
	void writeToFile(string inputFilename );

	~CSimulationParameters(void);
};

