#pragma once


#include <vector>
using namespace std;

#ifndef _USE_MATH_DEFINES
	#define _USE_MATH_DEFINES
#endif
#include <math.h>

class CMath
{

public:

	typedef  vector<vector<int>> MatrixInt;

	static double dist2(double x1,double y1,double x2,double y2);

	static double norm2squared(double x, double y);

	static void resizeMatrix(MatrixInt& M, int size1, int size2); 
	static void zeroMatrix(MatrixInt& M, int size1, int size2); 

	static double calculateTriArea(double* xTri, double* yTri);

	static double P(double xi, double eta,double* xTri);
	static double Q(double xi, double eta,double* yTri);
	static double P1(double xi, double eta, double* fe_P1_on_Tri);
	static double atan3(double x,double y);
	static double atan4(double x,double y);

	static void calc_vectors_sum(vector<double>& v1, vector<double>& v2, vector<double>& v_sum);

	CMath(void);
	~CMath(void);
};

