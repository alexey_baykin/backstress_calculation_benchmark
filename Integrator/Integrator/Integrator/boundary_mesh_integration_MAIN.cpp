
#include <iostream>
#include <vector>
#include <omp.h>

#include <direct.h>
using namespace std;

#include "ElastFundSolutionShifted.h"
#include "Mesh.h"
#include "FESpaceP1.h"
#include "Integrator2d.h"
#include "Utilities.h"

#include "Timer.h"

#include "int2d_whole_mesh.h"

void boundary_mesh_integration_MAIN(){

	int nx = 20*16;
	int ny = nx;

	double lambda = 1.0;
	double G = 1.0;
	CElastFundSolution::set_physical_constants(lambda, G);

	string nx_ny_str = CUtilities::get_nx_ny_string(nx, ny);

	string freeFEM_scripts_folder_path = "../FreeFEMScripts/";

	string freeFEm_output_folder_path = "../../../Output/";
	string simFolder = freeFEm_output_folder_path + "sim" + nx_ny_str + "/";
	string cpp_output_folder = simFolder + "cppOutput/";

	_mkdir(cpp_output_folder.c_str());

	//string mesh_filepath = freeFEM_scripts_folder_path + "mesh" + nx_ny_str + ".msh";
	string mesh_filepath = simFolder + "mesh" + nx_ny_str + ".msh";

	CMesh mesh;
	mesh.readMeshFromFile(mesh_filepath);
	int tri_number_in_mesh = mesh.triangles.size();


	vector<int> vertice_non_zero_label;

	mesh.get_vertice_indice_with_non_zero_label(vertice_non_zero_label);

	CFESpaceP0 bx(tri_number_in_mesh);
	CFESpaceP0 by(tri_number_in_mesh);

	double fpars[2];

	CTimer timer;

	int tid;

	timer.start();

	CIntegrator2d integrator2d;
	int gauss_quadrature_order = 8;
	integrator2d.set_gauss_quadrature_2d(gauss_quadrature_order);

	/* ******************************************************************** */
	/*                     FUNCTION INTEGRATION LOOP                        */
	/* ******************************************************************** */

	const int TIME_STEP_MAX = 1;

	int n = mesh.vertices.size();

	CFESpaceP1 ubXi(n);
	CFESpaceP1 vbXi(n);

	CFESpaceP1 sigmaxxbXi(n);
	CFESpaceP1 sigmayybXi(n);
	CFESpaceP1 sigmaxybXi(n);

	CFESpaceP1 ubEta(n);
	CFESpaceP1 vbEta(n);

	CFESpaceP1 sigmaxxbEta(n);
	CFESpaceP1 sigmayybEta(n);
	CFESpaceP1 sigmaxybEta(n);

	CFESpaceP1 ub(n);
	CFESpaceP1 vb(n);

	CFESpaceP1 sigmaxxb(n);
	CFESpaceP1 sigmayyb(n);
	CFESpaceP1 sigmaxyb(n);


	for (int it = 1; it <= TIME_STEP_MAX; it++){

		string time_step_str = CUtilities::int_2_str(it);

		//string bx_filepath = freeFEM_scripts_folder_path + "sin_exp_fe_P0" + nx_ny_str + ".fe";
		string bx_filepath = simFolder + "bx_timestep_" + time_step_str + ".fe";
		string by_filepath = simFolder + "by_timestep_" + time_step_str + ".fe";

		bx.read_FE_function_from_file(bx_filepath);
		by.read_FE_function_from_file(by_filepath);

		int2d_boundary_mesh(mesh,
			vertice_non_zero_label, 
			integrator2d,	
			vbXi,
			vbEta,
			bx, by);

		CMath::calc_vectors_sum(vbXi.dofs, vbEta.dofs, vb.dofs);

		/* Output for FreeFEM */
		string vb_filepath = cpp_output_folder + "vb_boundary_timestep_" + time_step_str + ".fe";
		vb.write_FE_function_to_file(vb_filepath);

	}
	

	cout << " Time Elapsed: " << timer.get_time_elapsed() << " ms" << endl;

	mesh.write_mesh_to_file(mesh_for_draw_filepath);
	vbEta.write_FE_function_to_file(fe_for_draw_filepath);

	callFreeFEM(freeFEMExePath, draw_fe_P1_script_filepath, true);


}