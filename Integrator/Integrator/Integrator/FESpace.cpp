#include "FESpace.h"


CFESpace::CFESpace()
{
}
void CFESpace::read_FE_function_from_file(string fe_filepath){

	ifstream fe_file(fe_filepath);

	int dofs_count;

	fe_file >> dofs_count;

	dofs.resize(dofs_count);

	for (int i = 0; i<dofs_count; i++){

		fe_file >> dofs[i];

	}

	fe_file.close();

};

void CFESpace::write_FE_function_to_file(string fe_filepath){

	ofstream fe_file(fe_filepath);

	int dofs_count = dofs.size();

	fe_file << dofs_count << endl;

	for (int i = 0; i<dofs_count; i++){

		fe_file << dofs[i] << " ";
                  
	}

	fe_file.close();

};

CFESpace::~CFESpace()
{
}
