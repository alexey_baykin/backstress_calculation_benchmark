
#include "int2d_whole_mesh.h"
#include <omp.h>

void int2d_whole_mesh_whole_sim(
	CMesh& mesh, CIntegrator2d& integrator2d,
	CFESpaceP1& ubXi,
	CFESpaceP1& vbXi,
	CFESpaceP1& sigmaxxbXi,
	CFESpaceP1& sigmayybXi,
	CFESpaceP1& sigmaxybXi,
	CFESpaceP1& ubEta,
	CFESpaceP1& vbEta,
	CFESpaceP1& sigmaxxbEta,
	CFESpaceP1& sigmayybEta,
	CFESpaceP1& sigmaxybEta,
	CFESpaceP0& bx, CFESpaceP0& by){

	int n = vbXi.dofs.size();

	double fpars[2];

	for (int iv = 0; iv < n; iv++){

		int* ipars = NULL;

		fpars[0] = mesh.vertices[iv].x;
		fpars[1] = mesh.vertices[iv].y;

		ubXi.dofs[iv] =  integrator2d.calc_int2d(CElastFundSolutionShifted::uXi, bx, mesh, ipars, fpars);
		vbXi.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::vXi, bx, mesh, ipars, fpars);

		sigmaxxbXi.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmaxxXi, bx, mesh, ipars, fpars);
		sigmayybXi.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmayyXi, bx, mesh, ipars, fpars);
		sigmaxybXi.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmaxyXi, bx, mesh, ipars, fpars);

		ubEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::uEta, by, mesh, ipars, fpars);
		vbEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::vEta, by, mesh, ipars, fpars);

		sigmaxxbEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmaxxEta, by, mesh, ipars, fpars);
		sigmayybEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmayyEta, by, mesh, ipars, fpars);
		sigmaxybEta.dofs[iv] = integrator2d.calc_int2d(CElastFundSolutionShifted::sigmaxyEta, by, mesh, ipars, fpars);

	}

}