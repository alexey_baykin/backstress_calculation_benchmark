#pragma once

#include <ctime>

class CTimer
{
public:

	double start_clock;
	
	void start();
	
	double get_time_elapsed();

	CTimer();
	~CTimer();
};

