#pragma once

#include <vector>
#include <fstream>

using namespace std;
class CFESpace
{
public:
	vector<double> dofs;

	CFESpace();

	void read_FE_function_from_file(string fe_filepath);
	void write_FE_function_to_file(string fe_filepath);
	virtual ~CFESpace();
};

