#include "SimulationParameters.h"


CSimulationParameters::CSimulationParameters(void)
{
}

void CSimulationParameters::generateOtherParameters(){

	kl = kr;

	mu 	= E/(2.0*(1.0+nu)); 			   /* [Pa], mu elastic modulus */
	lambda = E*nu/((1.0+nu)*(1.0-2.0*nu)); /* [Pa], lambda elastic modulus */
	
	a1 = 1.0;
	a2 = mu/lambda*a1;

	Nc = int(0.5*M_PI*R*NN/10);
	Nv = int(R*NN);
	Nf = int(kr*NN);
	Nr = int((R-kr)*NN);


}

void CSimulationParameters::generateOtherParameters2(){

	kl = kr;

	mu 	= E/(2.0*(1.0+nu)); 			   /* [Pa], mu elastic modulus */
	lambda = E*nu/((1.0+nu)*(1.0-2.0*nu)); /* [Pa], lambda elastic modulus */
	
	a1 = 1.0;
	a2 = mu/lambda*a1;

	Nc = int(R*NN/10);
	Nv = int(R*NN/1);
	Nf = int(kr*NN);
	Nr = int((R-kr)*NN);


}



void CSimulationParameters::writeToFile(string inputFilename ){

	ofstream inputFreeFEM(freeFEMScriptFolder + inputFilename);

	inputFreeFEM << "#R=" << endl;
	inputFreeFEM << R << endl;

	inputFreeFEM << "#kr=" << endl;
	inputFreeFEM << kr << endl;

	inputFreeFEM << "#Nc=" << endl;
	inputFreeFEM << Nc << endl;

	inputFreeFEM << "#Nv=" << endl;
	inputFreeFEM << Nv << endl;

	inputFreeFEM << "#Nf=" << endl;
	inputFreeFEM << Nf << endl;

	inputFreeFEM << "#Nr=" << endl;
	inputFreeFEM << Nr << endl;

	inputFreeFEM << "#xtip=" << endl;
	inputFreeFEM << xtip << endl;

	inputFreeFEM << "#ytip=" << endl;
	inputFreeFEM << ytip << endl;

	inputFreeFEM << "#Renr=" << endl;
	inputFreeFEM << Renr << endl;

	inputFreeFEM.close();

}
CSimulationParameters::~CSimulationParameters(void)
{
}
