#include "ElastFundSolutionShifted.h"


CElastFundSolutionShifted::CElastFundSolutionShifted()
{
}


double CElastFundSolutionShifted::uXi(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::uXi(fpars[0] - x, fpars[1] - y);

}
double CElastFundSolutionShifted::vXi(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::vXi(fpars[0] - x, fpars[1] - y);

}

double CElastFundSolutionShifted::sigmaxxXi(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::sigmaxxXi(fpars[0] - x, fpars[1] - y);

}

double CElastFundSolutionShifted::sigmayyXi(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::sigmayyXi(fpars[0] - x, fpars[1] - y);

}
double CElastFundSolutionShifted::sigmaxyXi(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::sigmaxyXi(fpars[0] - x, fpars[1] - y);

}



double CElastFundSolutionShifted::uEta(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::uEta(fpars[0] - x, fpars[1] - y);

}
double CElastFundSolutionShifted::vEta(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::vEta(fpars[0] - x, fpars[1] - y);

}

double CElastFundSolutionShifted::sigmaxxEta(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::sigmaxxEta(fpars[0] - x, fpars[1] - y);

}

double CElastFundSolutionShifted::sigmayyEta(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::sigmayyEta(fpars[0] - x, fpars[1] - y);

}
double CElastFundSolutionShifted::sigmaxyEta(double x, double y, int* ipars, double* fpars){

	return CElastFundSolution::sigmaxyEta(fpars[0] - x, fpars[1] - y);

}

CElastFundSolutionShifted::~CElastFundSolutionShifted()
{
}
