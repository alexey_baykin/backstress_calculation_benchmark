#pragma once

#include "allInclude.h"
#include "freeFEMScriptPaths.h"

class CSimulationParametersPUFEM
{
public:
	double R; /* Domain radius*/
	double kr;

	double E;  /* [Pa], Young's modulus */
	double nu; /* [], Poisson's ration */

	double mu;
	double lambda;

	double a1;
	double a2;

	int NoutR;
	int NoutL;
	int NoutT;
	int NoutF;
	int NoutC; /* Fracture closed part */

	int NinR;
	int NinL;
	int NinT;
	int NinF;
	int NinC; /* Fracture closed part */

	/* Labels */
	int outRLabel;
	int outLLabel;
	int outTLabel;
	int outFLabel;
	int outCLabel; /* Fracture closed part */

	int inRLabel;
	int inLLabel;
	int inTLabel;
	int inFLabel;
	int inCLabel; /* Fracture closed part */



	double xtip;
	double ytip;
	double Renr;

	CSimulationParametersPUFEM(void);

	void generateOtherParameters();

	void writeToFile(string inputFilename );
	~CSimulationParametersPUFEM(void);
};

