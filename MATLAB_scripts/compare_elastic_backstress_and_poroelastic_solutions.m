output_folder = '../Output/';

nx = 20*(2^1);
ny = nx;

sim_folder = [output_folder '/sim_nx' num2str(nx) 'ny_' num2str(ny) '/'];

matlab_input_folder = [sim_folder 'forMATLABOutput/'];


MAX_STEPS = 100;

for it=1:MAX_STEPS
    
    vElast_filepath = [matlab_input_folder 'vElast_timestep_'  num2str(it) '.matlab'];
    vb_filepath = [matlab_input_folder 'vb_timestep_'  num2str(it) '.matlab'];
    vPoro_filepath = [matlab_input_folder 'vPoro_timestep_'  num2str(it) '.matlab'];
    
    uElast_filepath = [matlab_input_folder 'uElast_timestep_'  num2str(it) '.matlab'];
    ub_filepath = [matlab_input_folder 'ub_timestep_'  num2str(it) '.matlab'];
    uPoro_filepath = [matlab_input_folder 'uPoro_timestep_'  num2str(it) '.matlab'];
    
    uElast = dlmread(uElast_filepath);
    ub = dlmread(ub_filepath);
    uPoro = dlmread(uPoro_filepath);
    
    vElast = dlmread(vElast_filepath);
    vb = dlmread(vb_filepath);
    vPoro = dlmread(vPoro_filepath);
    
    surf(uElast);
    hold on;
     surf(-ub);
%     hold on;
    %colormap winter
%     surf(-uPoro);
    hold off;
    
    zlim([-0.1,0.1]);
    
    drawnow;
    
%     surf(vElast+vb);
%     hold on;
%     surf(vb);
%     hold on;
%     surf(vPoro);
%     hold off;
    
end



